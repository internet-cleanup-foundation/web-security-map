# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-03 19:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: websecmap/app/dashboard.py:60
msgid "Content"
msgstr ""

#: websecmap/app/dashboard.py:68
msgid "Administration"
msgstr ""

#: websecmap/app/dashboard.py:76
msgid "Recent Actions"
msgstr ""

#: websecmap/app/dashboard.py:83
msgid "Rebuild Ratings"
msgstr ""

#: websecmap/app/dashboard.py:89
msgid "Task Processing Status (WIP)"
msgstr ""

#: websecmap/app/dashboard.py:95
msgid "websecmap resources"
msgstr ""

#: websecmap/app/dashboard.py:98
msgid "Gitlab Repository"
msgstr ""

#: websecmap/app/dashboard.py:103
msgid "Admin repository"
msgstr ""

#: websecmap/app/dashboard.py:108
msgid "websecmap Website"
msgstr ""

#: websecmap/app/dashboard.py:124
#, python-format
msgid "Recent Actions for %s"
msgstr ""

#: websecmap/settings.py:816
msgid "🔧 configuration"
msgstr ""

#: websecmap/settings.py:819
msgid "configuration"
msgstr ""

#: websecmap/settings.py:820
msgid "map configuration"
msgstr ""

#: websecmap/settings.py:821
msgid "import regions"
msgstr ""

#: websecmap/settings.py:824
msgid "🏢 organizations"
msgstr ""

#: websecmap/settings.py:832
msgid "🔬 scanners"
msgstr ""

#: websecmap/settings.py:842
msgid "🗺️ map (autogenerated)"
msgstr ""

#: websecmap/settings.py:847
msgid "🕒 periodic tasks"
msgstr ""

#: websecmap/settings.py:855
msgid "ℹ️ helpdesk"
msgstr ""

#: websecmap/settings.py:875
msgid "👾️ the game"
msgstr ""
