# docker-compose configuration to build a local development installation of the websecmap platform.

services:
  # message broker to distribute tasks
  broker:
    image: redis
    stop_signal: KILL  # tests run in ephemeral container, no need to wait for clean shutdown
    logging: {driver: none}
    # Not configuring persistent storage for broker. Restarting will cause all unfinished
    # tasks to be forgotten, instead of lingering around.
    ports:
      - 6379

  # stateful storage
  database:
    # latest mysql (8.x) causes errors, pin until these can be investigated
    # error: "Authentication plugin 'caching_sha2_password' cannot be loaded
    image: mysql:8
    stop_signal: KILL  # tests run in ephemeral container, no need to wait for clean shutdown
    logging: {driver: none}
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD:
      MYSQL_ROOT_PASSWORD: "${DB_ROOT_PASSWORD:-secret}"
      MYSQL_DATABASE: "${DB_NAME:-failmap}"
      MYSQL_USER: "${DB_USER:-failmap}"
      MYSQL_PASSWORD: "${DB_PASSWORD:-failmap}"
    ports:
      - 3306
    command:
      # make sure a sane character set and collation are used
      - --character-set-server=utf8
      - --collation-server=utf8_general_ci
    healthcheck:
        test: ["CMD", "mysqladmin", "ping", "-h", "localhost"]

  # task executer
  worker:
    image: "${IMAGE}"
    stop_signal: KILL  # tests run in ephemeral container, no need to wait for clean shutdown
    restart: unless-stopped
    links:
      - broker
      - database:mysql
    # celery dislikes running as root
    user: nobody
    environment:
      BROKER: redis://broker:6379/0
      DJANGO_DATABASE: production
      UWSGI_CHEAPER: "0"
      NETWORK_SUPPORTS_IPV6: 1
      # let celery be a little more informative regarding console messages
      TERM: xterm-color
    command: [ "celery", "worker", "-l", "info", "--pool", "gevent"]
    depends_on:
        database:
            condition: service_healthy

  # web interfaces
  admin:
    image: "${IMAGE}"
    stop_signal: KILL  # tests run in ephemeral container, no need to wait for clean shutdown
    restart: unless-stopped
    links:
      - broker
      - database:mysql
    environment:
      BROKER: redis://broker:6379/0
      ALLOWED_HOSTS: "${ALLOWED_HOSTS:-localhost,127.0.0.1,::1}"
      DJANGO_DATABASE: production
      UWSGI_CHEAPER: "0"
      # django decides what to log based on type of console
      TERM: xterm-color
    ports:
      - "8000"
    volumes:
      - ../websecmap:/source/websecmap
    command: production --migrate --loaddata development_user
    depends_on:
        database:
            condition: service_healthy
    healthcheck:
        test: ["CMD", "curl", "-f", "http://localhost:8000"]
        retries: 30
        timeout: 180s
        interval: 10s

  frontend:
    image: "${IMAGE}"
    stop_signal: KILL  # tests run in ephemeral container, no need to wait for clean shutdown
    restart: unless-stopped
    links:
      - broker
      - database:mysql
    environment:
      ALLOWED_HOSTS: "${ALLOWED_HOSTS:-localhost,127.0.0.1,::1}"
      DJANGO_DATABASE: production
      UWSGI_PYTHON_AUTORELOAD: "no"
      UWSGI_CHEAPER: "0"
      # django decides what to log based on type of console
      TERM: xterm-color
      SERVICE_NAME: websecmap-frontend
      APPLICATION_MODE: frontend
    ports:
      - "8000"
    command: production
