from datetime import timedelta

from celery import group

from websecmap.celery import app
from websecmap.scanners import plannedscan
from websecmap.scanners.autoexplain import add_bot_explanation
from websecmap.scanners.models import EndpointGenericScan
from websecmap.scanners.scanner import finish_those_that_wont_be_scanned, unique_and_random
from websecmap.scanners.scanner.autoexplain_trust_microsoft import (
    autoexplain_trust_microsoft_and_include_their_webserver_headers,
)

SCANNER = "autoexplain_no_https_possible"

query = EndpointGenericScan.objects.all().filter(
    type="plain_http",
    is_the_latest_scan=True,
    rating="no_https_redirect_and_no_https_on_standard_port",
)

# The PKI domain is excepted as it often hosted Certificate Revocation Lists
# The CRL domain is excepted for the same reason. Running https there could cause all kinds of confusion
# Note that "is public information so should be unencrypted" is a weak argument: integrity and confidentiality are
#  mandatory properties for public information.
excepted_subdomains = ["pki", "crl"]


@app.task(queue="kickoff", ignore_result=True)
def plan_scan():
    scans = query.filter(
        endpoint__url__computed_subdomain__in=excepted_subdomains, comply_or_explain_is_explained=False
    )
    urls = [endpoint_generic_scan.endpoint.url for endpoint_generic_scan in scans]
    plannedscan.request(activity="scan", scanner=SCANNER, urls=unique_and_random(urls))


@app.task(queue="kickoff")
def compose_planned_scan_task(**kwargs):
    urls = plannedscan.pickup(activity="scan", scanner=SCANNER, amount=kwargs.get("amount", 25))
    return compose_scan_task(urls)


def compose_scan_task(urls):
    scans = query.filter(endpoint__url__in=urls).only("id", "endpoint__url__id")
    finish_those_that_wont_be_scanned(SCANNER, scans, urls)

    return group(
        [
            scan.si(scan_id=endpoint_generic_scan.pk)
            | plannedscan.finish.si("scan", SCANNER, endpoint_generic_scan.endpoint.url.pk)
            for endpoint_generic_scan in list(set(scans))
        ]
    )


@app.task(queue="database", ignore_result=True)
def scan(scan_id: int):
    # don't try to explain scans that are already explained in the meanwhile(!) this task might be old and prior
    # tasks could already have explained this one.
    a_scan = EndpointGenericScan.objects.all().filter(id=scan_id, comply_or_explain_is_explained=False).first()
    if not a_scan:
        return

    # Only 1000: where there is NO https. 25 points is a redirect, which is not OK for PKI/CRL!
    if a_scan.rating != "no_https_redirect_and_no_https_on_standard_port":
        return

    if a_scan.endpoint.url.computed_subdomain not in excepted_subdomains:
        return

    # The plain_http rating is removed
    add_bot_explanation(a_scan, "domain_hosts_certificate_revocation_list", timedelta(days=365 * 10))

    # Also remove associated HSTS header rating, there will not be a HSTS of course for this domain.
    autoexplain_trust_microsoft_and_include_their_webserver_headers(a_scan)
