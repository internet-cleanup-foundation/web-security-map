from django.apps import AppConfig


class ScannersConfig(AppConfig):
    name = "websecmap.scanners"
