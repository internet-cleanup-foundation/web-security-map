# Generated by Django 3.2.16 on 2023-03-21 17:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0039_product_is_irrelevant"),
    ]

    operations = [
        migrations.AddField(
            model_name="product",
            name="open_cve_link",
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
