# Generated by Django 3.2.16 on 2022-11-23 16:51

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0023_auto_20221102_1242"),
    ]

    operations = [
        migrations.AddField(
            model_name="urlip",
            name="is_v4",
            field=models.BooleanField(default=True),
        ),
    ]
