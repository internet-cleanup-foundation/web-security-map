# Generated by Django 3.2.16 on 2023-01-17 14:07

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0025_alter_urlip_is_unused"),
    ]

    operations = [
        migrations.AlterField(
            model_name="plannedscan",
            name="finished_at_when",
            field=models.DateTimeField(blank=True, help_text="when finished, timeout, error", null=True),
        ),
    ]
