# Generated by Django 4.2.3 on 2023-12-23 16:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0092_cookiescansession_stop_reason"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cookiebanner",
            name="session_id",
            field=models.BigIntegerField(
                blank=True, help_text="Unique ID for the browser session the cookie banner was found", null=True
            ),
        ),
    ]
