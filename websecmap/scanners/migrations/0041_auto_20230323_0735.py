# Generated by Django 3.2.16 on 2023-03-23 07:35

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("taggit", "0005_auto_20220424_2025"),
        ("scanners", "0040_product_open_cve_link"),
    ]

    operations = [
        migrations.CreateModel(
            name="TagProxy",
            fields=[],
            options={
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("taggit.tag",),
        ),
        migrations.RenameField(
            model_name="product",
            old_name="name_slug",
            new_name="slug",
        ),
    ]
