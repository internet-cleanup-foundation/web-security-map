# Generated by Django 4.2.3 on 2023-07-27 09:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0064_domainmx"),
    ]

    operations = [
        migrations.CreateModel(
            name="GeoIpCorrection",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("source", models.CharField(blank=True, max_length=10, null=True)),
                ("network", models.CharField(blank=True, max_length=200, null=True)),
                ("country_code", models.CharField(blank=True, max_length=2, null=True)),
                ("response", models.TextField(blank=True, null=True)),
                ("at_when", models.DateTimeField(blank=True, null=True)),
                ("last_scan_moment", models.DateTimeField(blank=True, null=True)),
                ("is_the_latest", models.BooleanField(blank=True, default=False, null=True)),
            ],
        ),
    ]
