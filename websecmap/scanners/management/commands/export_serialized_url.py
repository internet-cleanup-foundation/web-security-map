import logging

from django.core.management.base import BaseCommand

from websecmap.scanners.housekeeping.export_serialized.organization import export_url
from websecmap.organizations.models import Url

log = logging.getLogger(__package__)


class Command(BaseCommand):
    help = "Export an url with all related data so it can be imported elsewhere."

    def add_arguments(self, parser):
        parser.add_argument("url_id", help="Url to export")
        super().add_arguments(parser)

    def handle(self, *args, **options):
        print(export_url(Url.objects.get(pk=options["url_id"])))
