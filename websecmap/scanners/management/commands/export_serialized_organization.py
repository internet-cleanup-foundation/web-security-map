import logging

from django.core.management.base import BaseCommand

from websecmap.scanners.housekeeping.export_serialized.organization import export_organization
from websecmap.organizations.models import Organization

log = logging.getLogger(__package__)


class Command(BaseCommand):
    help = "Export an organization with all related data so it can be imported elsewhere."

    def add_arguments(self, parser):
        parser.add_argument("organization_id", help="Organization to export")
        super().add_arguments(parser)

    def handle(self, *args, **options):
        print(export_organization(Organization.objects.get(pk=options["organization_id"])))
