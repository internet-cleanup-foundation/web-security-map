from datetime import datetime, timezone, timedelta

from freezegun import freeze_time

from websecmap.organizations.models import Url
from websecmap.scanners.models import UrlGenericScan
from websecmap.scanners.housekeeping.deduplicate import scans
from websecmap.scanners.models import Endpoint, EndpointGenericScan

import logging

log = logging.getLogger(__package__)


def test_reduce_normalized_identical_scans(db, default_policy, default_scan_metadata):
    # and a bunch of similar scans, some are explained, others are of a different type. They have a nice gradient
    # over time so we can see that some scans should be merged while others stay untouched.
    # The reduction algorithm is slow.
    # in short:

    # 01: jan 2021: test scan, out of scope, should not be touched
    # 02: feb 2021: dnssec scan (range 1), oldest one
    # 03: mar 2021: test scan, out of scope
    # 04: apr 2021: dnssec scan, a normalized duplicate of feb. The feb one should be merged into this one. This one
    # is explained, so the merged one should also be explained. (range 1)
    # 05: may 2021: dnssec scan, copy of the april one, so should have the feb 2021 scan date. End of range 1.
    # 06: jun 2021: test scan, out of scope, should not be touched
    # 07: jul 2021: other dnssec scan, range 2
    # 08: aug 2021: yet another dnssec scan (range 3), this one will be merged in the sept one
    # 09: sep 2021: latest dnssec scan from range 3.

    # in the end there should be 3 dnssec scans, where there where 6.

    # note that last_scan_moment has an auto_now_add, which is set with time freeze

    # Let's setup the scenario above:
    u1 = Url.objects.create(url="range1.nl")
    u2 = Url.objects.create(url="range2.foundation")
    u3 = Url.objects.create(url="range3.com")

    # Some things that does not match the policy
    # 01
    with freeze_time("2021-01-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="G",
            type="test",
            evidence="test",
            last_scan_moment=datetime(2021, 1, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 1, 1, tzinfo=timezone.utc),
        )

    # 02
    with freeze_time("2021-02-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="range 1",
            type="dnssec",
            evidence="range 1",
            last_scan_moment=datetime(2021, 2, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 2, 1, tzinfo=timezone.utc),
        )

    # 03
    with freeze_time("2021-03-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="G",
            type="test",
            evidence="test with another value",
            last_scan_moment=datetime(2021, 3, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 3, 1, tzinfo=timezone.utc),
        )

    # 04, duplicate of 02 but explained.
    with freeze_time("2021-04-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="range 1",
            type="dnssec",
            evidence="range 1",
            last_scan_moment=datetime(2021, 4, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 4, 1, tzinfo=timezone.utc),
            comply_or_explain_is_explained=True,
            comply_or_explain_explanation="explanation",
            comply_or_explain_explained_on=datetime(2021, 4, 1, tzinfo=timezone.utc),
        )

    # 05: and the copy of 4, this is the latest scan in range 1 and should be the one left. Not explained!
    # but the previous one was, so this one should be after merging.
    with freeze_time("2021-05-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="range 1",
            type="dnssec",
            evidence="range 1",
            last_scan_moment=datetime(2021, 5, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 5, 1, tzinfo=timezone.utc),
            is_the_latest_scan=True,
            comply_or_explain_is_explained=False,
        )

    # 06: out of scope
    with freeze_time("2021-06-01"):
        UrlGenericScan.objects.create(
            url=u1,
            rating="G",
            type="test",
            evidence="test",
            last_scan_moment=datetime(2021, 6, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 6, 1, tzinfo=timezone.utc),
        )

    # 07 the one at range 2 is just 1 scan and no duplication
    with freeze_time("2021-07-01"):
        UrlGenericScan.objects.create(
            url=u2,
            rating="range 2",
            type="dnssec",
            evidence="range 2",
            last_scan_moment=datetime(2021, 7, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 7, 1, tzinfo=timezone.utc),
            is_the_latest_scan=True,
        )

    # 08: duplicate of 09
    with freeze_time("2021-08-01"):
        UrlGenericScan.objects.create(
            url=u3,
            rating="range 3",
            type="dnssec",
            evidence="range 3",
            last_scan_moment=datetime(2021, 8, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 8, 1, tzinfo=timezone.utc),
        )

    # 09: leading scan for range 3
    with freeze_time("2021-09-01"):
        UrlGenericScan.objects.create(
            url=u3,
            rating="range 3",
            type="dnssec",
            evidence="range 3",
            is_the_latest_scan=True,
            last_scan_moment=datetime(2021, 9, 1, tzinfo=timezone.utc),
            rating_determined_on=datetime(2021, 9, 1, tzinfo=timezone.utc),
        )

    scans.deduplicate(u1)
    scans.deduplicate(u2)
    scans.deduplicate(u3)

    # Verify that there are indeed 3 dnssec scans, and those are now from feb (range 1), jul and aug.
    # all three should be the latest scans...
    assert UrlGenericScan.objects.filter(is_the_latest_scan=True, type="dnssec").count() == 3

    # no other dnssec scans:
    assert UrlGenericScan.objects.filter(type="dnssec").count() == 3

    # the other 3 test scans are not affected
    assert UrlGenericScan.objects.filter().count() == 6

    # range 3 started in aug and ended in september:
    r3_scan = UrlGenericScan.objects.filter(url=u3, type="dnssec").first()
    assert r3_scan.last_scan_moment == datetime(2021, 9, 1, tzinfo=timezone.utc)
    assert r3_scan.rating_determined_on == datetime(2021, 8, 1, tzinfo=timezone.utc)

    # range 1 started from feb and ended in may, is also explained
    r1_scan = UrlGenericScan.objects.filter(url=u1, type="dnssec").first()
    assert r1_scan.last_scan_moment == datetime(2021, 5, 1, tzinfo=timezone.utc)
    assert r1_scan.rating_determined_on == datetime(2021, 2, 1, tzinfo=timezone.utc)
    assert r1_scan.comply_or_explain_is_explained is True


def setup_endpoint_test():
    u1 = Url.objects.create(url="basisbeveiliging.nl")
    pr = {
        "protocol": "http",
        "port": 1,
        "ip_version": 4,
        "is_dead": True,
        "url": u1,
        "discovered_on": datetime(2021, 7, 3, tzinfo=timezone.utc),
    }
    e1 = Endpoint.objects.create(**pr)
    manager = EndpointGenericScan.objects

    return u1, e1, manager


def test_deduplicate_all_scans_sequentially_same_day(db):
    u1, e1, manager = setup_endpoint_test()

    # Straight forward test: Have two scans that are the same, they will be merged.
    now = datetime.now(timezone.utc)
    then = datetime.now(timezone.utc) - timedelta(days=1)
    # since they are both at the same time, the latest scan one is set to the latest scan, but which it is, is random.
    props = {
        "endpoint": e1,
        "rating": "1",
        "evidence": "1",
        "type": "a",
        "rating_determined_on": then,
        "last_scan_moment": now,
    }
    manager.create(**{**props, **{"comply_or_explain_is_explained": True, "comply_or_explain_explanation": "yolo"}})
    manager.create(**{**props, **{"is_the_latest_scan": True}})

    assert manager.count() == 2
    scans.deduplicate(u1)
    assert manager.count() == 1
    # Is the latest scan is ported

    scan = manager.all().first()
    log.info(scan.__dict__)
    assert scan.is_the_latest_scan is True
    assert scan.comply_or_explain_is_explained is True
    assert scan.comply_or_explain_explanation == "yolo"

    # Do it again, vice versa.
    # Now lets reverse the order of things scanned on the same day and see that stuff is copied correctly
    manager.all().delete()
    assert manager.count() == 0
    manager.create(**{**props, **{"is_the_latest_scan": True}})
    manager.create(**{**props, **{"comply_or_explain_is_explained": True, "comply_or_explain_explanation": "yolo"}})

    assert manager.count() == 2
    scans.deduplicate(u1)
    assert manager.count() == 1
    scan = manager.all().first()
    log.info(scan.__dict__)
    assert scan.comply_or_explain_is_explained is True
    assert scan.comply_or_explain_explanation == "yolo"
    assert scan.is_the_latest_scan is True


def test_deduplicate_all_scans_sequentially_with_intermediate_scan(db):
    # An intermediate scan that deviates from the current scan will not result in deletions.
    # scan 1 december: A
    # scan 2 december: B
    # scan 3 december: A
    # Conclusion: nothing is deleted because scan 2 is relevant

    u1, e1, manager = setup_endpoint_test()

    # Straight forward test: Have two scans that are the same, they will be merged.
    now3 = datetime.now(timezone.utc) - timedelta(days=13)
    now2 = datetime.now(timezone.utc) - timedelta(days=14)
    now1 = datetime.now(timezone.utc) - timedelta(days=15)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now3, last_scan_moment=now3)

    assert manager.count() == 3
    scans.deduplicate(u1)
    assert manager.count() == 3

    # Add another endpoint for another scan, this is not taken into consideration as the scan type is different
    manager.create(endpoint=e1, rating="A", evidence="1", type="b", rating_determined_on=now3, last_scan_moment=now3)
    scans.deduplicate(u1)
    assert manager.count() == 4

    # Add even more noise that is flipping between states, nothing will be deleted.
    now4 = datetime.now(timezone.utc) - timedelta(days=4)
    now5 = datetime.now(timezone.utc) - timedelta(days=5)
    now6 = datetime.now(timezone.utc) - timedelta(days=6)
    now7 = datetime.now(timezone.utc) - timedelta(days=7)
    now8 = datetime.now(timezone.utc) - timedelta(days=8)
    now9 = datetime.now(timezone.utc) - timedelta(days=9)
    now10 = datetime.now(timezone.utc) - timedelta(days=10)
    now11 = datetime.now(timezone.utc) - timedelta(days=11)
    now12 = datetime.now(timezone.utc) - timedelta(days=12)

    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now12, last_scan_moment=now12)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now11, last_scan_moment=now11)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now10, last_scan_moment=now10)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now9, last_scan_moment=now9)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now8, last_scan_moment=now8)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now7, last_scan_moment=now7)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now6, last_scan_moment=now6)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now5, last_scan_moment=now5)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now4, last_scan_moment=now4)

    scans.deduplicate(u1)
    assert manager.count() == 13


def test_deduplicate_all_scans_sequentially_with_noise(db):
    # Create a bunch of unique urls, endpoints and scans and verify that they are not deleted because they are all legit

    u1 = Url.objects.create(url="basisbeveiliging.nl")
    u2 = Url.objects.create(url="example.com")
    p1 = {
        "protocol": "http",
        "port": 1,
        "ip_version": 4,
        "is_dead": True,
        "url": u1,
        "discovered_on": datetime(2021, 7, 3, tzinfo=timezone.utc),
    }
    p2 = {
        "protocol": "http",
        "port": 1,
        "ip_version": 4,
        "is_dead": True,
        "url": u2,
        "discovered_on": datetime(2021, 7, 3, tzinfo=timezone.utc),
    }
    e1 = Endpoint.objects.create(**p1)
    e2 = Endpoint.objects.create(**p1)
    e3 = Endpoint.objects.create(**p1)
    e4 = Endpoint.objects.create(**p2)
    e5 = Endpoint.objects.create(**p2)
    e6 = Endpoint.objects.create(**p2)
    manager = EndpointGenericScan.objects

    now2 = datetime.now(timezone.utc) - timedelta(days=2)
    now1 = datetime.now(timezone.utc) - timedelta(days=3)
    manager.create(endpoint=e1, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e1, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e2, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e2, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e3, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e3, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e4, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e4, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e5, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e5, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e6, rating="A", evidence="1", type="a", rating_determined_on=now1, last_scan_moment=now1)
    manager.create(endpoint=e6, rating="B", evidence="1", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e6, rating="A", evidence="2", type="a", rating_determined_on=now2, last_scan_moment=now2)
    manager.create(endpoint=e6, rating="A", evidence="1", type="b", rating_determined_on=now2, last_scan_moment=now2)

    assert manager.count() == 14
    scans.deduplicate(u1)
    scans.deduplicate(u2)
    assert manager.count() == 14


def test_deduplicate_all_scans_sequentially_in_a_sequence(db):
    u1, e1, manager = setup_endpoint_test()
    td = timedelta

    # See if the whole sequence is merged into one
    now = datetime.now(timezone.utc)
    props = {"endpoint": e1, "rating": "1", "evidence": "1", "type": "a"}
    # last_scan_moment on new objects cannot be set, need to override it manually
    # auto_now_add: Note that the current date is always used; it’s not just a default value that you can override.
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=9)}})
    a.last_scan_moment = now - td(days=9)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=8)}})
    a.last_scan_moment = now - td(days=8)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=7)}})
    a.last_scan_moment = now - td(days=7)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=6)}})
    a.last_scan_moment = now - td(days=6)
    a.save()
    # The explanation is somewhere in the middle
    a = manager.create(
        **{
            **props,
            **{
                "rating_determined_on": now - timedelta(days=5),
                "last_scan_moment": now - timedelta(days=5),
                "comply_or_explain_is_explained": True,
                "comply_or_explain_explanation": "yolo",
            },
        }
    )
    a.last_scan_moment = now - td(days=5)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=4)}})
    a.last_scan_moment = now - td(days=4)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=3)}})
    a.last_scan_moment = now - td(days=3)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=2)}})
    a.last_scan_moment = now - td(days=2)
    a.save()
    a = manager.create(**{**props, **{"rating_determined_on": now - td(days=1), "is_the_latest_scan": True}})
    a.last_scan_moment = now - td(days=1)
    a.save()

    assert manager.count() == 9
    scans.deduplicate(u1)
    assert manager.count() == 1

    scan = manager.all().first()
    log.info(scan.__dict__)
    assert scan.is_the_latest_scan is True
    assert scan.comply_or_explain_is_explained is True
    assert scan.comply_or_explain_explanation == "yolo"
    # make sure the last scan moment is also get migrated correctly
    assert scan.last_scan_moment == now - td(days=1)
