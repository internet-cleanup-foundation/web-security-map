from websecmap.scanners.housekeeping.tasks import perform_housekeeping
from websecmap.organizations.models import Url
from websecmap.scanners.models import Endpoint, EndpointGenericScan
from datetime import datetime, timezone, timedelta
from freezegun import freeze_time


def test_perform_housekeeping_reductions(db, default_scan_metadata, default_policy):

    # let's make 5 identical endpoints, add several identical scans to them.
    # this should be reduced to 1 endpoint and one scan.

    url = Url.objects.create(url="example.nl")

    with freeze_time("2024-01-01"):

        endpoints = []
        endpoint_scans = []
        for day in range(5):
            ep = Endpoint.objects.create(
                url=url,
                protocol="http",
                port=80,
                ip_version=4,
                is_dead=True,
                discovered_on=datetime.now(timezone.utc) + timedelta(days=day),
            )
            endpoints.append(ep.id)

            epgs = EndpointGenericScan.objects.create(
                endpoint=ep,
                type="plain_http",
                evidence="example",
                rating="25",
                rating_determined_on=datetime.now(timezone.utc),
            )
            endpoint_scans.append(epgs.id)

        # make sure one endpoint is still alive...
        ep.is_dead = False
        ep.save()

        result = perform_housekeeping(url.id)

        assert Endpoint.objects.all().count() == 1
        assert EndpointGenericScan.objects.all().count() == 1

        assert result == {
            "before": {
                "moments": [
                    # datetimes are serialized to isoformat so they can be stored in the database as a result.
                    "2024-01-01T00:00:00+00:00",
                    # Why are these not moments? Because we cannot have multiple of the same scan types per moment
                    # datetime(2024, 1, 2, tzinfo=timezone.utc),
                    # datetime(2024, 1, 3, tzinfo=timezone.utc),
                    # datetime(2024, 1, 4, tzinfo=timezone.utc),
                    # datetime(2024, 1, 5, tzinfo=timezone.utc)
                ],
                "happenings": {
                    "dead_endpoints": endpoints[0:4],
                    "dead_urls": [],
                    "endpoint_scans": endpoint_scans,
                    "non_resolvable_urls": [],
                    "url_scans": [],
                },
                "endpoints": 5,
                "metrics": {"endpoint": 5, "url": 0},
            },
            "after": {
                # oldest scan stays
                "moments": ["2024-01-01T00:00:00+00:00"],
                "happenings": {
                    "dead_endpoints": [],
                    "dead_urls": [],
                    # Endpoint scans are removed from old to new, so the highest id stays, in this case that is '5'
                    # (this id will differ when the whole test suite is run)
                    "endpoint_scans": [endpoint_scans[4]],
                    "non_resolvable_urls": [],
                    "url_scans": [],
                },
                "endpoints": 1,
                "metrics": {"endpoint": 1, "url": 0},
            },
            "comparison": {
                # four endpoints have been removed
                "endpoints": -4,
                "metrics": {
                    # four endpoint scans have been removed
                    "endpoint": -4,
                    "url": 0,
                },
            },
        }

    # the one endpoint saved has the oldest discovery date:
    assert Endpoint.objects.first().discovered_on.isoformat() == "2024-01-01T00:00:00+00:00"


def test_perform_housekeeping_cleaning(db):
    # add several metrics that *could* be cleaned so the cleaning function is triggered.
    ...
