import logging
import json

from websecmap.scanners.housekeeping.deduplicate import endpoints, scans
from websecmap.scanners.housekeeping.clean import (
    location_third_party_website_content,
    http_security_header_strict_transport_security,
    plain_http,
    dnssec,
)

from websecmap.organizations.models import Url
from websecmap.reporting.report import significant_moments
from websecmap.scanners.scanmetadata import get_backend_scanmetadata
from websecmap.scanners.models import Endpoint, UrlGenericScan, EndpointGenericScan

log = logging.getLogger(__name__)


"""
Outage: a disruption of service that caused an endpoint or url to die. After the outage a new, similar endpoint is
created. Before ±2022 outages caused new endpoints with roughly the same metrics. After that, before creating a new
endpoint a check was performed to see if a recently deceased endpoint could be revived. While that has saved a ton of
useless endpoints from then on, there are still useless duplicate endpoints in the database.

Clean: cleans an existing metric in the database.

Reduce: reduces some value to a more generic one. This is not normalization as data is lost.

Deduplicate: removes a duplicate record from the database
"""


def perform_housekeeping(url_id: int):
    url = Url.objects.all().filter(id=url_id).first()
    if not url:
        return None

    before = create_tracing_data(url)

    # todo: move origin inside of endpoint, check if all fields are migrated.
    # todo: test this on a larger swath of production data locally, and see if this cleans up enough.
    endpoints.deduplicate(url, outage_days=365)

    cleaners = [
        location_third_party_website_content,
        http_security_header_strict_transport_security,
        plain_http,
        dnssec,
    ]
    for cleaner in cleaners:
        cleaner.clean(url)

    scans.deduplicate(url)

    after = create_tracing_data(url)

    # so this can be stored in UrlGenericScan and tested for.
    return {"before": before, "after": after, "comparison": compare_tracing_data(before, after)}


def create_tracing_data(url: Url):
    metadata = get_backend_scanmetadata()

    moments, happenings = significant_moments([url], metadata["published_scan_types"])
    happenings["dead_endpoints"] = [scan.id for scan in happenings["dead_endpoints"]]
    happenings["dead_urls"] = [scan.id for scan in happenings["dead_urls"]]
    happenings["non_resolvable_urls"] = [scan.id for scan in happenings["non_resolvable_urls"]]
    happenings["endpoint_scans"] = [scan.id for scan in happenings["endpoint_scans"]]
    happenings["url_scans"] = [scan.id for scan in happenings["url_scans"]]
    return {
        "moments": [moment.isoformat() for moment in moments],
        "happenings": happenings,
        "endpoints": Endpoint.objects.all().filter(url=url).count(),
        "metrics": {
            "endpoint": EndpointGenericScan.objects.all().filter(endpoint__url=url).count(),
            "url": UrlGenericScan.objects.all().filter(url=url).count(),
        },
    }


def compare_tracing_data(before: dict, after: dict):
    return {
        "endpoints": after["endpoints"] - before["endpoints"],
        "metrics": {
            "endpoint": after["metrics"]["endpoint"] - before["metrics"]["endpoint"],
            "url": after["metrics"]["url"] - before["metrics"]["url"],
        },
    }


def summarize_housekeeping_results(organization_id: int):
    ugss = UrlGenericScan.objects.all().filter(url__organization__id=organization_id, type="housekeeping")

    summary = {
        "endpoints": 0,
        "metrics": {
            "endpoint": 0,
            "url": 0,
        },
    }
    for ugs in ugss:
        evidence = json.loads(ugs.evidence)
        summary["endpoints"] += evidence.get("comparison", {}).get("endpoints", 0)
        summary["metrics"]["endpoint"] += evidence.get("comparison", {}).get("metrics", {}).get("endpoint", 0)
        summary["metrics"]["url"] += evidence.get("comparison", {}).get("metrics", {}).get("url", 0)

    return summary
