"""
Helps cleanup the database of non-normalized records of scanners that had non-normalized results.

This is probably a duplicate and can be removed without issues...
"""

import logging
from typing import Union

from websecmap.scanners.models import EndpointGenericScan, UrlGenericScan
from websecmap.scanners.scanmetadata import get_backend_scanmetadata
from websecmap.organizations.models import Url
from websecmap.scanners.models import Endpoint
from collections import defaultdict
from typing import List

log = logging.getLogger(__name__)


def get_model_from_scan_type(scan_type):
    metadata = get_backend_scanmetadata()
    return EndpointGenericScan if scan_type in metadata["endpoint_scan_types"] else UrlGenericScan


def deduplicate(url: Url):
    # url scans are directly coupled to urls, but an url can, and will, have multiple endpoints.
    # in order for this approach to work, scans need to be requested per endpoint.

    # - means desc, so from new to old, and when they are on the same day, then from highest id to lowest id.
    deduplicate_set_of_scans(UrlGenericScan.objects.all().filter(url=url).order_by("-rating_determined_on", "-id"))

    for endpoint in Endpoint.objects.all().filter(url=url):
        deduplicate_set_of_scans(
            EndpointGenericScan.objects.all().filter(endpoint=endpoint).order_by("-rating_determined_on", "-id")
        )


def deduplicate_set_of_scans(all_scans: List[Union[EndpointGenericScan, UrlGenericScan]]):
    # prevent dozens of queries, we already have all scans now, so why re-query?:
    scans_per_scan_type = defaultdict(list)
    for scan in all_scans:
        scans_per_scan_type[scan.type].append(scan)

    for scans in scans_per_scan_type.values():
        deduplicate_scans_per_type(scans)


def deduplicate_scans_per_type(scans: List[Union[EndpointGenericScan, UrlGenericScan]]):
    # scan types are all the same here, so we can safely dedupe.

    merge_proposals = []

    # compare the current item in the scans list with the next one, and see if scans_are_identical, if so
    # create a merge proposal. These proposals can then be merged with ease.
    for i, current_scan in enumerate(scans[:-1]):
        previous_scan = scans[i + 1]

        if scan_is_identical(current_scan, previous_scan):
            log.debug("Proposing to merge scans: %s and %s", current_scan, previous_scan)
            merge_proposals.append({"from": previous_scan, "to": current_scan})

    log.debug("Found %s merge proposals", len(merge_proposals))

    # merge proposals are performed from old to new, as older scans will be deleted and thus cannot be merged anymore
    for proposal in reversed(merge_proposals):
        merge_scans(proposal["to"], proposal["from"])


def scan_is_identical(
    newer_scan: Union[EndpointGenericScan, UrlGenericScan], older_scan: Union[EndpointGenericScan, UrlGenericScan]
) -> bool:
    # See scanmanager.py when something is new / identical
    if newer_scan.rating != older_scan.rating:
        return False
    if newer_scan.evidence != older_scan.evidence:
        return False
    if newer_scan.explanation != older_scan.explanation:
        return False
    # omitting empty string values and {}
    if newer_scan.meaning and older_scan.meaning and newer_scan.meaning != older_scan.meaning:
        return False

    return True


def merge_scans(
    newer_scan: Union[EndpointGenericScan, UrlGenericScan], older_scan: Union[EndpointGenericScan, UrlGenericScan]
):
    log.debug("Merging scans: %s and %s", newer_scan, older_scan)

    newer_scan = set_newer_scan_fields(newer_scan, older_scan)

    newer_scan.save()
    older_scan.delete()


def set_newer_scan_fields(newer_scan, older_scan):

    # the oldest scan is when the rating was determined. Implicitly in the new scan the 'last scan moment' is set:
    newer_scan.rating_determined_on = older_scan.rating_determined_on

    # if one of these scans was the latest scan, so is the merged one of course.
    if newer_scan.is_the_latest_scan or older_scan.is_the_latest_scan:
        newer_scan.is_the_latest_scan = True

    # if there was an explanation on either, keep at least one:
    if older_scan.comply_or_explain_is_explained and not newer_scan.comply_or_explain_is_explained:
        newer_scan.comply_or_explain_is_explained = True
        newer_scan.comply_or_explain_explanation = older_scan.comply_or_explain_explanation
        newer_scan.comply_or_explain_explained_on = older_scan.comply_or_explain_explained_on
        newer_scan.comply_or_explain_explained_by = older_scan.comply_or_explain_explained_by
        newer_scan.comply_or_explain_explanation_valid_until = older_scan.comply_or_explain_explanation_valid_until
        newer_scan.comply_or_explain_case_additional_notes = older_scan.comply_or_explain_case_additional_notes
        newer_scan.comply_or_explain_case_handled_by = older_scan.comply_or_explain_case_handled_by

    return newer_scan
