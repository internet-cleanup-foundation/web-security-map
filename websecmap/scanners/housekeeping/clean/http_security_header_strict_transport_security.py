import json
import logging

from websecmap.organizations.models import Url
from websecmap.scanners.models import EndpointGenericScan
from websecmap.scanners.scanner.security_headers import reduce_url

log = logging.getLogger(__name__)


def clean(url: Url):
    scans = EndpointGenericScan.objects.all().filter(
        endpoint__url=url, type="http_security_header_strict_transport_security"
    )

    for scan in scans:
        if not scan.evidence:
            return

        scan.evidence = reduce_hsts_evidence(scan.evidence)
        scan.save(update_fields=["evidence"])


def try_get_json(data):
    try:
        return json.loads(data)
    except json.decoder.JSONDecodeError:
        return None


def reduce_hsts_evidence(evidence: str) -> str:
    # example_hsts_evidence = [
    #     {
    #         "request": 1,
    #         "url": "https://mijn.preprod1.digid.nl:443/",
    #         "protocol": "https",
    #         "status_code": 302,
    #         "content_type": "text/html; charset=utf-8",
    #         "headers": {
    #             "content-security-policy": "default-src 'self'; base-uri 'self'; font-src 'self' data:; img-src 'self
    #             "content-type": "text/html; charset=utf-8",
    #             "location": "https://preprod1.digid.nl/?check_cookie=true&process=my_digid&url=https%3a%2f%2fmijn.pre
    #             "permissions-policy": "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(),
    #             "referrer-policy": "same-origin",
    #             "strict-transport-security": "max-age=31536000 ; includesubdomains",
    #             "x-content-type-options": "nosniff",
    #             "x-frame-options": "sameorigin",
    #             "x-xss-protection": "1; mode=block",
    #         },
    #     },
    #     {
    #         "request": 2,
    #         "url": "https://preprod1.digid.nl/?check_cookie=true&process=my_digid&url=https%3A%2F%2Fmijn.preprod1.dig
    #         "protocol": "https",
    #         "status_code": 302,
    #         "content_type": "text/html; charset=utf-8",
    #         "headers": {
    #             "content-security-policy": "default-src 'self'; base-uri 'self'; font-src 'self' data:; img-src 'self
    #             "content-type": "text/html; charset=utf-8",
    #             "location": "https://preprod1.digid.nl/cookies_geblokkeerd?process=my_digid&url=https%3a%2f%2fmijn.pr
    #             "permissions-policy": "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(),
    #             "referrer-policy": "same-origin",
    #             "strict-transport-security": "max-age=31536000 ; includesubdomains",
    #             "x-content-type-options": "nosniff",
    #             "x-frame-options": "sameorigin",
    #             "x-xss-protection": "1; mode=block",
    #         },
    #     },
    # ]

    requests = try_get_json(evidence)
    if not requests:
        log.debug("No requests 1")
        return evidence

    if len(requests) == 0:
        log.debug("No requests 2")
        return evidence

    if "status_code" not in requests[0]:
        # probably another format
        log.debug("Incorrect format")
        return evidence

    for request in requests:
        request["url"] = reduce_url(request["url"])
        new_headers = {}
        if "strict-transport-security" in request["headers"]:
            new_headers["strict-transport-security"] = request["headers"].pop("strict-transport-security")

        if "location" in request["headers"]:
            new_headers["location"] = reduce_url(request["headers"].pop("location"))

        request["headers"] = new_headers

    return json.dumps(requests)
