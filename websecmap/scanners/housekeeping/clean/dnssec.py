from websecmap.organizations.models import Url
from websecmap.scanners.models import UrlGenericScan
import re


def clean(url: Url):
    scans = UrlGenericScan.objects.all().filter(
        url=url,
        type="dnssec",
    )

    for scan in scans:
        if not scan.evidence:
            return

        scan.evidence = clean_dnssec_evidence(scan.evidence)
        scan.save(update_fields=["evidence"])


def clean_dnssec_evidence(evidence: str) -> str:
    # first format lines, so extra spaces are gone:
    lines = evidence.splitlines()
    lines = [line.strip() for line in lines]

    # remove empty ines:
    lines = filter(lambda x: not re.match(r"^\s*$", x), lines)

    # remove the timestamp each line
    lines = [re.sub(r"^[ 0-9.]+: ", "", line).strip() for line in lines]

    # EXTRA_PROCESSING is returned in random order. Sort these lines, inject the sorted lines in the right spot.
    ip_lines = [line for line in lines if line.startswith("INFO [DNSSEC:EXTRA_PROCESSING]")]
    ip_lines.sort()
    print(ip_lines)
    new_lines = []
    injected = False
    for line in lines:
        if line.startswith("INFO [DNSSEC:EXTRA_PROCESSING]"):
            if not injected:
                injected = True
                new_lines.extend(ip_lines)
            # otherwise id
        else:
            new_lines.append(line)
    lines = new_lines

    # RRSIG_EXPIRES_AT has a date string every few days.
    # INFO [DNSSEC:RRSIG_EXPIRES_AT] Wed Oct 11 04:30:43 2023
    lines = [
        "INFO [DNSSEC:RRSIG_EXPIRES_AT] **DATE**," if line.startswith("INFO [DNSSEC:RRSIG_EXPIRES_AT]") else line
        for line in lines
    ]

    return "\n".join(lines)
