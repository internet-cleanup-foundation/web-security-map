import json
from datetime import datetime, timezone

from websecmap.organizations.models import Url
from websecmap.scanners.models import Endpoint, EndpointGenericScan, ScanType, ScanTypeCategory
from websecmap.scanners.scanner.accessibility_axe import store_axe_results, import_axe_scan_types


def test_store_axe_results(db, current_path):
    # load axe_results_icf.json
    with open(f"{current_path}/websecmap/scanners/tests/accessibility_axe/axe_results_icf.json") as f:
        axe_data = json.load(f)

    url = Url.objects.create(url="zutphen.nl")
    ep = Endpoint.objects.create(
        url=url, discovered_on=datetime.now(timezone.utc), port=443, protocol="https", ip_version=4
    )

    store_axe_results(ep.id, axe_data["violations"])

    # see if there are two issues, as there are two categories
    assert EndpointGenericScan.objects.filter(endpoint_id=ep.id).count() == 2

    # test the name of the issue
    first = EndpointGenericScan.objects.filter(endpoint_id=ep.id, type="accessibility_axe_color_contrast").first()

    # name of the rating contains axe:
    assert first.rating == "accessibility_axe_serious"

    # and see if the content is somewhat okay, has multiple pages and such:
    evidence = json.loads(first.evidence)
    assert len(evidence) == 7

    # this will change...
    assert evidence[0]["path"] == ["/", "clicked some page"]
    assert evidence[0]["violations"][0] == {
        "all": [],
        "any": [
            {
                "data": {
                    "bgColor": "#fafbfc",
                    "contrastRatio": 1.01,
                    "expectedContrastRatio": "4.5:1",
                    "fgColor": "#f8f9fa",
                    "fontSize": "9.0pt (12px)",
                    "fontWeight": "normal",
                    "messageKey": None,
                    "shadowColor": None,
                },
                "id": "color-contrast",
                "impact": "serious",
                "message": "Element has insufficient color contrast of 1.01 (foreground "
                "color: #f8f9fa, background color: #fafbfc, font size: 9.0pt "
                "(12px), font weight: normal). Expected contrast ratio of 4.5:1",
                "relatedNodes": [
                    {
                        "html": '<article class="entry-card card-content post-3604 post '
                        "type-post status-publish format-standard "
                        'has-post-thumbnail hentry category-updates" '
                        'data-reveal="bottom:no">',
                        "target": [
                            ".post-3604",
                        ],
                    },
                    {
                        "html": '<body class="home page-template-default page page-id-2800 '
                        "wp-custom-logo wp-embed-responsive elementor-default "
                        "elementor-kit-2783 elementor-page elementor-page-2800 "
                        "ct-elementor-default-template e--ua-blink e--ua-mac "
                        'e--ua-webkit" data-link="type-2" '
                        'data-prefix="single_page" data-header="type-1" '
                        'data-footer="type-1" itemscope="itemscope" '
                        'itemtype="https://schema.org/WebPage" '
                        'data-elementor-device-mode="desktop">',
                        "target": [
                            "body",
                        ],
                    },
                ],
            },
        ],
        "failureSummary": "Fix any of the following:\n"
        "  Element has insufficient color contrast of 1.01 (foreground color: "
        "#f8f9fa, background color: #fafbfc, font size: 9.0pt (12px), font weight: "
        "normal). Expected contrast ratio of 4.5:1",
        "html": '<time class="ct-meta-element-date" ' 'datetime="2024-12-24T16:00:03+01:00">24 dec 2024</time>',
        "impact": "serious",
        "none": [],
        "target": [
            'time[datetime="2024-12-24T16:00:03+01:00"]',
        ],
    }


def test_import_axe_scan_types(db):
    import_axe_scan_types()
    assert ScanType.objects.filter(name="accessibility_axe_tabindex").exists()

    # more than just accessibility...
    assert ScanTypeCategory.objects.all().count() > 1

    st = ScanType.objects.filter(name="accessibility_axe_page_has_heading_one").first()
    # accessibility + the two loaded in the rule definition
    assert st.category.count() == 3

    assert ScanType.objects.all().count() == 100
