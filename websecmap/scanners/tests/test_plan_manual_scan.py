import pytest
from websecmap.scanners.models import PlannedScan, Scanner
from websecmap.organizations.models import Url
from django.core.management import call_command, CommandError


def test_plan_manual_scan(db, default_scan_metadata, default_policy, caplog):
    """
    Test that a planned scan can be created.
    """

    # create a scanner
    scanner = Scanner.objects.all().create(python_name="testscanner")

    url = Url.objects.all().create(url="example.com")

    # call the command
    call_command("plan_manual_scan", ["example.com", "testscanner"])

    # check if the planned scan was created
    planned_scan = PlannedScan.objects.all().last()

    assert planned_scan.url.url == url.url
    assert planned_scan.scanner.python_name == scanner.python_name

    # the scanner should exist:
    # expect a django CommandError when a non-existing scanner is used
    with pytest.raises(CommandError):
        call_command("plan_manual_scan", ["example.com", "not_existing_scanner"])
        assert "usage: websecmap plan_manual_scan" in caplog.text

    # url should exist
    call_command("plan_manual_scan", ["not_existing_domain.com", "testscanner"])
    assert "No URL found for" in caplog.text
