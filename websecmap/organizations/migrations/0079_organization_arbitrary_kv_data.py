# Generated by Django 4.2 on 2023-06-20 10:42

import jsonfield.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0078_alter_organization_structured_reasoning_for_registrant_decisions"),
    ]

    operations = [
        migrations.AddField(
            model_name="organization",
            name="arbitrary_kv_data",
            field=jsonfield.fields.JSONField(
                blank=True,
                default={},
                help_text="Any other data that is extracted from the datasource, in key value pairs. It depends on the datasource imported. Every time of organization has their own unique codes.",
            ),
        ),
    ]
