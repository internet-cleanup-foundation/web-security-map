# Generated by Django 4.2.3 on 2024-04-10 09:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("organizations", "0084_remove_organization_type"),
    ]

    operations = [
        migrations.AlterField(
            model_name="organization",
            name="city",
            field=models.CharField(
                blank=True,
                default="",
                help_text="The city is part of the natural key for humans to find the right organizations when multiple organizations have the same name. This happens with primary schools a lot. There are even multiple ones with the same name in the same city. If that really becomes a problem a burough or district field should be added for further specification. For rare cases this field can just be used to contain city and disctrict. Like: Amsterdam (Zuid-Oost).",
                max_length=150,
            ),
        ),
    ]
