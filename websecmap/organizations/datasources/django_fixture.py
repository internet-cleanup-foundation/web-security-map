import logging

from websecmap.organizations.datasources.utils import finish_import
from websecmap.organizations.models import GenericFileDataset
from django.core import management
import os
import pathlib
from tempfile import gettempdir

log = logging.getLogger(__name__)

FILE_FORMAT = "django_fixture"


def import_django_fixture(dataset: GenericFileDataset):
    if dataset.is_imported:
        return

    # stdout on a NamedTemporaryFile does not work. So here is the non-fun workaround code.
    file_location = os.path.join(gettempdir(), "tmp.json")
    with open(file_location, "w", encoding="UTF-8") as f:
        management.call_command("loaddata", dataset.file.path, verbosity=1, stdout=f)

    dataset.state = pathlib.Path(file_location).read_text(encoding="UTF-8")
    finish_import(dataset)
