import json

from datetime import datetime, timezone
from websecmap.organizations.models import GenericFileDataset, Organization, Url
from django.core.files.uploadedfile import SimpleUploadedFile
from websecmap.organizations.datasources.rdapslurper_domains import import_rdapslurper_datasets


def test_import_dataset(db):

    org = Organization.objects.create(name="Test", country="NL", created_on=datetime.now(timezone.utc))

    # create a nice dataset to work with:
    sample_data = [
        {"organisation_id": org.id, "url": "example.nl"},
        {"organisation_id": org.id, "url": "example2.nl"},
    ]
    dataset = create_upload_file("file_format", sample_data)

    import_rdapslurper_datasets()

    assert Url.objects.all().count() == 2
    assert Url.objects.all().first().url == "example.nl"

    # adding it again does not change anything.
    dataset.state = ""
    dataset.save()
    import_rdapslurper_datasets()
    import_rdapslurper_datasets()
    assert Url.objects.all().count() == 2


def create_upload_file(file_format, data):
    now = datetime.now(timezone.utc).isoformat()

    idataset = GenericFileDataset()
    idataset.file_format = "rdapslurper_urls"
    idataset.is_imported = False
    idataset.file = SimpleUploadedFile(f"{file_format}{now}.json", bytes(json.dumps(data), encoding="utf-8"))
    idataset.origin = f"Automated test on {datetime.now(timezone.utc).isoformat()}."
    idataset.state = ""
    idataset.save()

    return idataset
