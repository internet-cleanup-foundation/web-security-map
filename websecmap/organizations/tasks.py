from websecmap.organizations.datasources import digitoegankelijk_2024, nl_gov_rio_2024
from websecmap.organizations.whois import sidn

# register tasks
__all__ = [sidn, digitoegankelijk_2024, nl_gov_rio_2024]
