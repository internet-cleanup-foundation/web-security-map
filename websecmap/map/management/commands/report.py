# For a complete map update run: report, report_organizations, report_map
import logging

from websecmap.app.management.commands._private import GenericTaskCommand
from websecmap.map import report

log = logging.getLogger(__name__)


class Command(GenericTaskCommand):
    """Rebuild url ratings (fast) and add a report for today if things changed. Creates stats for two days."""

    help = __doc__

    def handle(self, *args, **options):
        try:
            self.scanner_module = report
            super().handle(self, *args, **options)
        except KeyboardInterrupt:
            log.info("Received keyboard interrupt. Stopped.")
