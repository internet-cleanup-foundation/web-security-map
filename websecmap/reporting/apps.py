from django.apps import AppConfig


class ReportingConfig(AppConfig):
    name = "websecmap.reporting"
