"""
Reports are stored to disk and retrieved from disk transparently using the fasted JSON library we can find.

Reports can be stored in various formats, where the "original" is the original from websecmap. A variant might be
added without detailed findings for example.
"""

import gzip
import json
import logging
from json import JSONDecodeError
from typing import Dict, Type, Union
from pathlib import Path
from django.conf import settings

# Models import is used in type hints
from django.db.models import Model  # pylint: disable=unused-import

from websecmap.celery import app
from websecmap.map.models import OrganizationReport
import glob
import os

log = logging.getLogger(__package__)


def store_report(report_id: Union[int, str], model: str = "OrganizationReport", data: Dict = None):
    if data is None:
        data = {}
    return _write_json_to_gzip(location_on_disk(report_id, model), data)


def retrieve_report(report_id: Union[int, str], model: Union[str, Type["Model"]] = "OrganizationReport"):
    return _read_json_from_gzip(location_on_disk(report_id, model))


def location_on_disk(report_id: Union[int, str], model: Union[str, Type["Model"]] = "OrganizationReport") -> Path:
    # prevent some path traversal and directory issues, but not all:
    report_id = str(report_id).replace(".", "").replace("/", "").replace("\\", "")

    # because of the 500.000 reports on basisbeveiliging, which take 100 gigabyte, we should use some sort of mapping
    # that create subdirectories per 1000 files or so. Otherwise administration with linux tools is too slow.
    if len(report_id) <= 3:
        return storage_location_of_model(model) / f"{report_id}.gson"

    # Any report ids larger than 999 (or fff) will get their own subdirectory based on their first 3 characters:
    subdirectory = report_id[:3]
    return storage_location_of_model(model) / subdirectory / f"{report_id}.gson"


def storage_location_of_model(model: Union[str, Type["Model"]] = "OrganizationReport"):
    model_name = model if isinstance(model, str) else model.__name__
    return get_original_storage_location() / model_name


def get_original_storage_location():
    return settings.REPORT_STORAGE_DIR / "original"


def sync_reports_from_db_to_disk(model: Type["Model"] = OrganizationReport, limit: int = 10000000):
    """
    Copies all reports to disk and deletes them from the database.

    Don't forget to optimize tables afterwards otherwise disk space is not reclaimed.
    """

    for report in model.objects.all().only("id")[:limit]:
        sync_report_from_db_to_disk(report.id, model)


def sync_report_from_db_to_disk(report_id: int, model: Type["Model"] = OrganizationReport):
    # Moves a report to file and deletes the report data from the database.
    report = model.objects.all().filter(id=report_id).first()
    if not report or report.calculation is None:
        log.debug("Report contains no data (anymore), not syncing it to disk.")
        return

    _write_json_to_gzip(location_on_disk(report_id, model), report.calculation)

    report.calculation = None
    report.save()


def _write_json_to_gzip(location, content):
    # Just crash horribly when the disk is full.
    # log.debug(f"Writing report file: {location}")
    location.parent.mkdir(parents=True, exist_ok=True)

    with gzip.open(location, "wt") as file:
        file.write(json.dumps(content))


def _read_json_from_gzip(location):
    try:
        # log.debug(f"Reading report file: {location}")
        with gzip.open(location, "rt") as file:
            return json.loads(file.read())
    except FileNotFoundError:
        log.info("Report does not exist on location %s.", location)
        return {}
    except JSONDecodeError:
        # this is weird, should not happen. Don't crash.
        log.error("Report does not contain valid json, %s.", location)
        return {}
    except Exception as e:  # pylint: disable=broad-except
        # Possibly corrupted gzip file, this might happen for whatever reason.
        # Error -3 while decompressing data: invalid stored block lengths
        log.exception("Other error occurred when reading report from %s: %s", location, e)
        return {}


@app.task(queue="storage", ignore_result=True)
def clean_unlinked_reports(model: Type["Model"] = OrganizationReport) -> int:
    # A subtle approach to clean unlinked reports. When a report record is deleted, the linked file will stay on disk.
    # This is probably a feature of Django and not a bug. Working with files is also hard and not fun.

    # For some reason reports are re-created and old reports are not deleted. That's not our biggest problem as that
    # only eats up a bit of disk space. But on the long run it eats up a lot, so this routine can be used to clean
    # old reports to free up disk space. For example: the first run there where about 800.000 reports on disk that
    # where not linked. This has freed up about 20 gigabytes of disk space.

    # The approach here is to compare all actual files against expected filed. If there are more actual files,
    # those files will be baleeted.
    log.info("Cleaning up unneeded files from type %s", model)

    # get all reports on disk, we expect there are more of these than there are in the database
    storage_location = storage_location_of_model(model)
    log.debug("Looking in report storage location: %s", storage_location)
    actual_report_files = glob.glob(f"{storage_location}/**/*", recursive=True)
    actual_report_files = [file for file in actual_report_files if file.endswith(".gson")]
    log.debug("Found %s actual report files.", len(actual_report_files))

    # get all reports in the database
    reports_ids_in_database = model.objects.all().only("id").values_list("id", flat=True)
    expected_report_files = [location_on_disk(report_id, model) for report_id in reports_ids_in_database]
    # can't compare posixpath to string, so convert posixpath to string:
    expected_report_files = [str(file) for file in expected_report_files]
    # make sure that these are actual gson files and not directories
    expected_report_files = [file for file in expected_report_files if file.endswith(".gson")]
    log.debug("Found %s actual expected files.", len(expected_report_files))

    if not expected_report_files or not actual_report_files:
        log.info(
            "No reports found in database or storage location. Expected: %s, Present: %s",
            len(expected_report_files),
            len(actual_report_files),
        )
        return 0

    log.debug("First actual report location: %s", actual_report_files[0])
    log.debug("First expected report location: %s", expected_report_files[0])

    # delete files that are not in the expected list
    reports_to_delete = list(set(actual_report_files) - set(expected_report_files))
    log.debug("Found %s reports to delete.", len(reports_to_delete))
    log.warning("Going to remove %s unlinked reports.", len(reports_to_delete))
    for unlinked_report in reports_to_delete:
        # make sure to remove files, not directories that might contain other relevant reports
        if os.path.isfile(unlinked_report):
            log.debug("Deleting unlinked report: %s", unlinked_report)
            os.unlink(unlinked_report)

    log.info("Cleaned up %s unlinked reports.", len(reports_to_delete))

    return len(reports_to_delete)
