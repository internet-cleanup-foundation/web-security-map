from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse

from .models import ScanSession, Violations


@admin.register(ScanSession)
class ScanSessionAdmin(admin.ModelAdmin):
    show_full_result_count = False

    list_display = ("url", "stop_reason", "violation_count", "get_violations", "at_when", "session_id")
    search_fields = ("url", "session_id")
    list_filter = ["url", "stop_reason", "violation_count", "at_when"][::-1]

    @admin.display(description="Violations")
    def get_violations(self, obj):
        return format_html(
            "".join(
                f"""<a href={reverse(f'admin:{rel._meta.app_label}_{rel._meta.model_name}_change', args=[rel.id])}>
                {str(rel)}
                </a>"""
                for rel in obj.violations.all()
            )
        )


@admin.register(Violations)
class ViolationsAdmin(admin.ModelAdmin):
    list_display = ("id", "get_scansession", "filename", "download")

    search_fields = ("filename",)

    list_filter = ("filename",)

    @admin.display(description="Scan session")
    def get_scansession(self, obj):
        return format_html(
            "".join(
                f"""<a href={reverse(f'admin:{rel._meta.app_label}_{rel._meta.model_name}_change', args=[rel.id])}>
                {str(rel)}</a>"""
                for rel in obj.scansession_set.all()
            )
        )

    @admin.display(description="Download file")
    def download(self, obj):
        if obj.file_object:
            return format_html(f'<a href="{obj.file_object.url}" download>{obj.filename}</a>')
        return ""
