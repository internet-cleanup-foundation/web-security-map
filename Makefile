SHELL=/bin/bash

# settings
app_name = websecmap

VIRTUAL_ENV=.venv

# variables for environment
bin = ${VIRTUAL_ENV}/bin

# shortcuts for common used binaries
uv = uv # provided by flake.nix
python = ${bin}/python
pip = ${bin}/pip
pip-compile = ${bin}/pip-compile
pip-sync = ${bin}/pip-sync

# python binary used to create virtual environment
base_python ?= python3.10

# application binary
app = ${bin}/${app_name}

ifeq (${MAKECMDGOALS},)
$(info Virtualenv path: ${VIRTUAL_ENV})
$(info Run App: websecmap)
$(info )
$(info Run `make help` for available commands or use tab-completion.)
$(info )
$(info Running complete setup and test of development environment now.)
$(info )
endif

pysrcdirs = ${app_name}/ tests/
pysrc = $(shell find ${pysrcdirs} -name \*.py)
shsrc = $(shell find * ! -path vendor\* -name \*.sh)

.PHONY: test check check_migrations setup run fix autofix clean mrproper test_integration

# default action to run
all: setup test check

# setup entire dev environment
setup: ${app}	## setup development environment and application
	@test \! -z "$$PS1" || (echo -ne "Development environment is tested and ready."; \
	if command -v websecmap &>/dev/null;then \
		echo -e " Development shell is activated."; \
	else \
		echo -e "\n\nTo activate development shell run:"; \
		echo -e "\n\t. ${VIRTUAL_ENV}/bin/activate$$([[ "$$SHELL" =~ "fish" ]] && echo .fish)\n"; \
		echo -e "Or refer to Direnv in README.md for automatic activation."; \
	fi)

# install application and all its (python) dependencies
${app}: ${VIRTUAL_ENV}/.requirements.installed | ${uv}
	# install project and its dependencies
	uv pip install --no-deps --editable .
	@test -f $@ && touch $@  # update timestamp, do not create empty file

# run tests with coverage, and generate coverage report
coverage: enable_coverage=coverage run -m
coverage: test
	# generate pretty html coverage `htmlcov/index.html`
	coverage html
	# generate coverage report
	coverage report

tests ?= .

test: ## run test suite
	# run testsuite
	# this replaces "make testcase case=test_plannedscan" with  "make test tests=test_plannedscan"
	${enable_coverage} pytest --verbose --doctest-modules --doctest-ignore-import-errors --nomigrations -vv -ra -k 'not integration_celery and not integration_scanners and not system and ${tests}' ${testargs}

doctest: testargs=./websecmap
doctest: test

# all quality assurance commands before pushing new code
qa: autofix test

check: check_py check_migrations check_sh lint  ## code quality checks
check_py: ${pysrc} ${app}
	# check code quality
	# TOML file config are ignored since 8.4.1 so use direct params :(
	pylama --max-line-length=120 --ignore="E252,W605,E203" --skip="*/migrations/*,vendor/*,docs/*,build/*,.*/*" --linters="pycodestyle,pyflakes" .
	# check formatting
	black --check .

check_migrations: ${pysrc} ${app}
	# ensure no model updates are commited without migrations
	${app} makemigrations --check
	# check if all migrations apply cleanly
	DB_NAME=:memory: ${app} migrate --verbosity=0

check_sh: ${shsrc}
	# shell script checks (if installed)
	if command -v shellcheck &>/dev/null;then shellcheck --version; shellcheck ${shsrc}; fi

autofix fix: .make.fix  ## automatic fix of trivial code quality issues
.make.fix: ${pysrc} ${app}
	# remove unused imports
	autoflake .
	# autoformat code
	black .
	# do a check after autofixing to show remaining problems
	${MAKE} check
	@touch $@  # update timestamp

run: ${app}  ## run complete application stack (frontend, worker, broker)
	# start server (this can take a while)
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} devserver

audit: ${app}
	${python} -m bandit --configfile pyproject.toml --recursive websecmap

run_no_backend: ${app}  ## run application stack without broker/worker
	# start server (this can take a while)
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} devserver --no-backend

run-frontend: ${app}  ## only run frontend component
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} runserver

run-nonlocal-frontend: ${app}  ## only run frontend component
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} runserver 0.0.0.0:8000

app: ${app}  ## perform arbitrary app commands
    # make app cmd="help"
    # make app cmd="report -y municipality"
    # make app cmd="makemigrations"
    # make app cmd="migrate"
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} ${cmd}

run-worker: ${app}  ## only run worker component
	DEBUG=1 NETWORK_SUPPORTS_IPV6=1 ${app} celery worker -ldebug

run-broker:  ## only run broker
	docker run --rm --name=redis -p 6379:6379 redis

run-tool-playwright-privacy:
	docker run --rm --name=playwright-privacy -p 31337:31337 \
		registry.gitlab.com/internet-cleanup-foundation/playwright-privacy/main:latest

test_integration: ${app}
	# run integration tests
	# see docs at make test why the -k option is so explicit (no globbing anymore).
	DB_NAME=test.sqlite3 \
		pytest -vv -ra -k 'integration_celery or integration_scanners' ${testargs}

test_system:
	@if [ -z "$$IMAGE" ];then echo "Must provide Docker image variable IMAGE, to test against!"; exit 1; fi
	# run system tests
	pytest --verbose tests/system ${testargs}

test_datasets: ${app}
	/bin/sh -ec "find websecmap -path '*/fixtures/*.json' -print0 | \
		xargs -0n1 basename -s .yaml | uniq | \
		xargs -n1 ${app} test_dataset"

test_deterministic: | ${VIRTUAL_ENV}
	/bin/bash tools/compare_differences.sh HEAD HEAD tools/show_ratings.sh testdata

test_mysql:
	docker run --name mysql -d --rm -p 3306:3306 \
		-e MYSQL_ROOT_PASSWORD=failmap \
		-e MYSQL_DATABASE=failmap \
		-e MYSQL_USER=failmap \
		-e MYSQL_PASSWORD=failmap \
		mysql:8
	DJANGO_DATABASE=production DB_USER=root DB_HOST=127.0.0.1 \
		$(MAKE) test; e=$$?; docker stop mysql; exit $$e

test_postgres:
	docker run --name postgres -d --rm -p 5432:5432 \
		-e POSTGRES_DB=failmap \
		-e POSTGRES_USER=root \
		-e POSTGRES_PASSWORD=failmap \
		postgres:12
	DJANGO_DATABASE=production DB_ENGINE=postgresql_psycopg2 DB_USER=root DB_HOST=127.0.0.1 \
		$(MAKE) test; e=$$?; docker stop postgres; exit $$e

loaddata_testdata: db.sqlite3 | ${app}  ## load enough data to give a working websecmap instance
	${app} migrate
	${app} loaddata testdata

dumpdata_testdata: | ${app}
	${app} dumpdata  dumpdata organizations scanners map reporting --indent 2 > websecmap/organizations/fixtures/testdata.json

clean:  ## cleanup build artifacts, caches, databases, etc.
	# remove python cache files
	-find * -name __pycache__ -print0 | xargs -0 rm -rf
	# remove state files
	-rm -f .make.*
	# remove test artifacts
	-rm -rf .pytest_cache htmlcov/
	# remove build artifacts
	-rm -rf *.egg-info dist/ pip-wheel-metadata/
	# remove runtime state files
	-rm -rf *.sqlite3

clean_virtualenv:  ## cleanup virtualenv and installed app/dependencies
	# remove virtualenv
	-rm -fr ${VIRTUAL_ENV}/
	-rm .venv

mrproper: clean clean_virtualenv ## thorough cleanup, also removes virtualenv

${VIRTUAL_ENV}/.requirements.installed: requirements.txt requirements-dev.txt | ${uv} ${python}
	uv pip sync $^
	@touch $@  # update timestamp

requirements = requirements.txt requirements-dev.txt
requirements: ${requirements}

# perform 'pip freeze' on first class requirements in .in files.
requirements.txt: requirements.in | ${uv}
	${uv} pip compile ${pip_compile_args} --custom-compile-command="make requirements" --no-strip-extras --output-file $@ $<

requirements-dev.txt: requirements-dev.in requirements.in | ${uv}
	${uv} pip compile ${pip_compile_args} --custom-compile-command="make requirements" --no-strip-extras --output-file $@ $<

pip-sync: | ${python}
	# synchronizes the .venv with the state of requirements.txt
	${uv} pip sync requirements.txt requirements-dev.txt

update_requirements: pip_compile_args=--upgrade
update_requirements: _mark_outdated requirements.txt requirements-dev.txt _commit_update

upgrade_package: | ${uv} ## upgrade a single Python package in requirements.txt
	@if [ -z "${package}" ];then echo "Usage: make upgrade_package package=package-name"; exit 1; fi
	${uv} pip compile requirements.in --upgrade-package ${package}

	upgrade_dev_package: | ${uv} ## upgrade a single Python package in requirments-dev.txt
	@if [ -z "${package}" ];then echo "Usage: make upgrade_dev_package package=package-name"; exit 1; fi
	${uv} pip compile requirements-dev.in --upgrade-package ${package}

_mark_outdated:
	touch requirements*.in

_commit_update: requirements.txt
	git add requirements*.txt requirements*.in
	git commit -m "Updated requirements."

${python}:
	# create virtualenv, Python version is determined by pyproject.toml requires-python
	${uv} venv

${uv}:


# utility
help:           ## Show this help.
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
	printf "\nRun \`make\` with any of the targets below to reach the desired target state.\n" ; \
	printf "\nTargets are complementary. Eg: the \`run\` target requires \`setup\` which is automatically executed.\n\n" ; \
	printf "%-30s %s\n" "target" "help" ; \
	printf "%-30s %s\n" "------" "----" ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$':' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf '\033[36m'; \
		printf "%-30s %s" $$help_command ; \
		printf '\033[0m'; \
		printf "%s\n" $$help_info; \
	done

check-commit: fix test

lint:  ${pysrc} ${app} ## Do basic linting
	pylint ${pysrcdirs}

isort:  ${pysrc} ${app}
	isort -rc ${pysrcdirs}

DEFAULT_IMAGE=websecmap
BUILD_TARGET=release

docker-build:
	docker build --target ${BUILD_TARGET} --cache-from registry.gitlab.com/internet-cleanup-foundation/web-security-map:latest --tag ${DEFAULT_IMAGE} .

docker-shell:
	docker run -ti --rm --entrypoint /bin/bash ${DEFAULT_IMAGE}

COMPOSE_FILE=tests/docker-compose.yml
COMPOSE_CMD=IMAGE=${DEFAULT_IMAGE} docker compose -f ${COMPOSE_FILE}

docker-compose-up: service=
docker-compose-up:
	${COMPOSE_CMD} up ${service}
