# Gitlabs new early alpha security features, see https://gitlab.com/help/user/application_security/index.md
# Disabled to reduce build times.
# include:
# - template: License-Scanning.gitlab-ci.yml
# - template: Dependency-Scanning.gitlab-ci.yml
# - template: Container-Scanning.gitlab-ci.yml
# - template: SAST.gitlab-ci.yml

variables:
  CI_IMAGE: ghcr.io/astral-sh/uv:python3.10-bookworm
  DOCKER_CI_IMAGE: websecmap/ci:docker-git
  DOCKER_SERVICE_VERSION: 20.10.16-dind
  UV_CACHE_DIR: .uv-cache

stages:
  - test_build
  - production

.test_template: &test_template
  stage: test_build

  image: $CI_IMAGE

  cache:
    # cache pip cache to make installs quicker
    - paths:
        - ".pip-cache"
      key: pip-cache
    # cache virtualenv because it does not change when requirements files don't change
    - paths:
        - ".virtualenv"
      key:
        files:
          - requirements.txt
          - requirements-dev.txt
    - paths:
        - $UV_CACHE_DIR
      key:
        files:
        - uv.lock

  before_script:
    - export VIRTUAL_ENV=".venv"
    - export PIP_CACHE_DIR="$CI_PROJECT_DIR/.pip-cache"
    - export PIP_DISABLE_PIP_VERSION_CHECK=1
    # prevent .txt requirements rebuilding when .in files have newer timestamps
    - touch requirements*.txt
    - make setup

  script:
    - export PATH=$VIRTUAL_ENV/bin:$PATH
    - export DJANGO_SETTINGS_MODULE=websecmap.settings
    # run desired make target
    - time make $MAKE_TARGETS

  only:
      - master
      - merge_requests
  retry: 1

# functional testing
test:
  <<: *test_template
  variables:
    MAKE_TARGETS: test

# check code quality
check:
  <<: *test_template
  variables:
    MAKE_TARGETS: check

# functional testing using mysql database instead of sqlite
test_mysql: &test_mysql_template
  <<: *test_template

  services:
    # minimal version required for django 4.2 = 8.0
    - mysql:8

  variables:
    # configuration for docker mysql service
    MYSQL_ROOT_PASSWORD: failmap
    MYSQL_DATABASE: failmap
    MYSQL_USER: failmap
    MYSQL_PASSWORD: failmap
    # select mysql database settings, all settings default to 'failmap'
    # so they don't need to be passed explicitly
    DJANGO_DATABASE: production
    DB_USER: root
    # only run tests, not code quality tests, as they are not affected by database
    MAKE_TARGETS: test

# functional testing using postgres database instead of sqlite
test_postgres: &test_postgres_template
  <<: *test_template

  services:
    # minimum version required for django 4.2 = 12
    - postgres:12

  variables:
    # configuration for docker postgres service
    POSTGRES_DB: failmap
    POSTGRES_USER: failmap
    POSTGRES_PASSWORD: failmap
    # select mysql database settings, all settings default to 'failmap'
    # so they don't need to be passed explicitly
    DJANGO_DATABASE: production
    DB_ENGINE: postgresql_psycopg2
    DB_HOST: postgres
    # only run tests, not code quality tests, as they are not affected by database
    MAKE_TARGETS: test

# Disabled due to build failures on a weird error:
# ALTER TABLE `scanners_endpointgenericscan` ADD COLUMN `evidence` varchar(9001) DEFAULT '0' NOT NULL
# MySQLdb._exceptions.OperationalError: (1118, 'Row size too large. The maximum row size for the used table type,
#    not counting BLOBs, is 65535. This includes storage overhead, check the manual.
#    You have to change some columns to TEXT or BLOBs'
# 9001 is well below 65535, so there is something wrong with the check.
# https://mariadb.com/kb/en/varchar/ -> also says it's ok.
# # functional testing using mariadb as it is default for debian 9
# test_mariadb: &test_mariadb_template
#   <<: *test_template
#   services:
#     - mariadb:latest
#   variables:
#     # configuration for docker mariadb service
#     MYSQL_ROOT_PASSWORD: failmap
#     MYSQL_DATABASE: failmap
#     MYSQL_USER: failmap
#     MYSQL_PASSWORD: failmap
#     # select mysql database settings, all settings default to 'failmap'
#     # so they don't need to be passed explicitly
#     DJANGO_DATABASE: production
#     DB_HOST: mariadb
#     DB_USER: root
#   retry: 1

# integration tests
integration:
  <<: *test_template
  stage: test_build

  services:
    - redis:latest

  variables:
    BROKER: redis://redis:6379/0
    # required to run celery worker under docker 'root' user
    C_FORCE_ROOT: 1
    MAKE_TARGETS: test_integration

dataset:
  <<: *test_template
  variables:
    MAKE_TARGETS: test_datasets

dataset_mysql:
  <<: *test_mysql_template
  variables:
    MAKE_TARGETS: test_datasets
  # run long tests only on master
  only: [master]

dataset_postgres:
  <<: *test_postgres_template
  variables:
    MAKE_TARGETS: test_datasets
  # run long tests only on master
  only: [master]

# test building image & run image smoketest
image_test:
  stage: test_build

  image: $DOCKER_CI_IMAGE

  services:
    - docker:$DOCKER_SERVICE_VERSION

  variables:
    SYSTEM_TEST_TIMEOUT: "120"
    GIT_SUBMODULE_STRATEGY: recursive

  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

  script:
    - tools/version.sh > version
    - tag=$(cat version|tr + _)

    - IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:$tag
    # push additional image keyed on the merge request ID, so a next build in this merge request can use cache
    - MR_CACHE_IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:mr-$CI_MERGE_REQUEST_IID
    # pull cache from the last build 'production' version
    - MASTER_CACHE_IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:latest

    # already pull external images for test_system while building app image
    - IMAGE=$IMAGE docker-compose --file tests/docker-compose.yml pull --quiet broker database &

    # build docker image to test building, using cache from previous builds, creating new cache metadata in image and pushing image to cache
    - DOCKER_BUILDKIT=1 time docker buildx build --cache-from $IMAGE --cache-from $MR_CACHE_IMAGE --cache-from $MASTER_CACHE_IMAGE --build-arg BUILDKIT_INLINE_CACHE=1 --build-arg VERSION=$(cat version) --target release --tag $IMAGE --tag $MR_CACHE_IMAGE --push .

    # run system tests against image
    - IMAGE=$IMAGE time make test_system

  # run on merge request to determine if build will not break on master
  only: [merge_requests]
  retry: 1

# create & test distributable release
build_test_publish:
  stage: test_build

  image: $DOCKER_CI_IMAGE

  services:
    - docker:$DOCKER_SERVICE_VERSION

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CACHE_IMAGE: $CI_REGISTRY/internet-cleanup-foundation/web-security-map:latest

  before_script:
    - echo "$DOCKER_REGISTRY_PASSWORD" | docker login --username "$DOCKER_REGISTRY_USER" --password-stdin
    - echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

  script:
    # store current version as artifact to copy into docker container
    - tools/version.sh > version
    - tag=$(cat version|tr + _)

    - IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:latest
    - BUILD_IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:$tag
    - CACHE_IMAGE=$CI_REGISTRY/internet-cleanup-foundation/web-security-map:$tag

    # build and push docker image for publishing, using cache from previous builds, creating new cache metadata in image
    - DOCKER_BUILDKIT=1 docker buildx build --cache-from $BUILD_IMAGE --cache-from $IMAGE --build-arg BUILDKIT_INLINE_CACHE=1 --build-arg VERSION=$(cat version) --target release --tag $IMAGE --tag $BUILD_IMAGE --push .

    # run system tests against image
    - IMAGE=$IMAGE time make test_system

  only:
    - master
    - tags
  retry: 1
